public class StudentRecord
{
	private String name;
	private String address;
	private int age;
	private double mathGrade;
	private double englishGrade;
	private double scienceGrade;
	private double average;
	private static int STUDENTCOUNT;


	public StudentRecord() //konstruk default
	{
		STUDENTCOUNT++;		
	}

	public void setName(String temp)
	{
		name = temp;
	}

	public String getAddress()
	{
		return address;
	}
	
	public void setAddress(String temp)
	{
		address = temp;
	}
	
	public int getAge()
	{
		return age;
	}

	public void setAge(int temp)
	{
		age = temp;
	}

	public double getAverage()
	{
		double result = 0;
		result = (mathGrade+englishGrade+scienceGrade)/3;
		return result;
	}

	public static int getStudentRecord()
	{
		return STUDENTCOUNT;
	}

	public void print(String tmp)
	{
		System.out.println("Name : "+name);
		System.out.println("Address : "+address);
		System.out.println("Age : "+age);
	}

	public void print(double eGrade , double mGrade, double sGrade)
	{
		System.out.println("Name : "+name);
		System.out.println("math grade : "+mathGrade);
		System.out.println("English Grade : "+englishGrade);
		System.out.println("Science Grade : "+scienceGrade);
	}
	public double getEnglishGrade()
	{
		return englishGrade;
	}

	public void setEnglishGrade(double englishGrade)
	{
		this.englishGrade = englishGrade;
	}

	public double getMathGrade()
	{
		return mathGrade;
	}

	public void setMathGrade(double tmp)
	{
		this.mathGrade = tmp;
	}

	public double getScienceGrade()
	{
		return scienceGrade;
	}

	public void setSienceGrade(double tmp)
	{
		this.scienceGrade = tmp;
	}

}