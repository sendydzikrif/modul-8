public class PublicElevatorTest{
	public static void main(String [] args){
		PublicElevator pubElevator = new PublicElevator();
		pubElevator.bukaPintu = true; //Penumpang Masuk
		pubElevator.bukaPintu = false; //Pintu ditutup

		//pergi ke lantai 0 dibawah gedung
		pubElevator.lantaiSkrg--;
		pubElevator.lantaiSkrg++;

		//lompat ke lantai 7 (hanya ada 5 lantai dalam gedung)
		pubElevator.lantaiSkrg = 7;
		pubElevator.bukaPintu = true; //penumpang masuk/keluar
		pubElevator.bukaPintu = false;
		pubElevator.lantaiSkrg = 1; //menuju lantai pertama
		pubElevator.bukaPintu = true; //penumpang masuk/keluar
		pubElevator.lantaiSkrg++; //elevator bergerak tanpa menutup pintu
		System.out.println("Lantai sekarang adalah " +pubElevator.lantaiSkrg);
		pubElevator.lantaiSkrg = 7;
		pubElevator.bukaPintu = false;
		pubElevator.lantaiSkrg--;
		pubElevator.lantaiSkrg--;
		System.out.println("Lantai sekarang adalah " +pubElevator.lantaiSkrg);
	}

}
