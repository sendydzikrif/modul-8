public class StudentRecordExample
{
	public static void main(String[] args)
	{
		StudentRecord annaRecord = new StudentRecord();
		StudentRecord beahRecord = new StudentRecord();
		StudentRecord crisRecord = new StudentRecord();

		annaRecord.setName("Anna");
		beahRecord.setName("Beah");
		crisRecord.setName("cris");

		//menampilkan
		System.out.println(annaRecord.getName() );
		System.out.println(beahRecord.getName() );
		System.out.println(crisRecord.getName() );

		//menampilkan jumlah siswa
		System.out.println("Count = "+StudentRecord.getStudentCount());
	}
}