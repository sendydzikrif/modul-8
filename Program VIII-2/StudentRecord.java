
public class StudentRecord
{
	public String name;
	public static int STUDENTCOUNT;
	public String address;
	public double englishGrade;
	public double mathGrade;
	
	public void setName(String nama)
	{
		this.name = nama;
	}
	
	
	public String getName()
	{
		return name;
	}
	public StudentRecord(String temp) 
	{
		this.name = temp;
	}
	public StudentRecord()
	{
		STUDENTCOUNT++;
	}
	public StudentRecord(String name, String Address)
	{
		this.name = name;
		this.address = Address;
		STUDENTCOUNT++;
	}
	public StudentRecord(double mGrade, double eGrade, double sGrade)
	{
		mathGrade = mGrade;
		englishGrade = eGrade;
		STUDENTCOUNT++;
	}
	public static int getStudentCount()
	{
		return STUDENTCOUNT;
	}

}

